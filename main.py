# parametres

mean_ages = {'a': 10, 'b': 15}
age_sds = {'a': 3, 'b': 3}
prob_same_mate = 0.4
prob_diff_mate = 0.4
start_a_num = 100
start_b_num = 100
time_step = 1
generations = 100
diff_mate_prob_A = 0.5
result_file = './1-0-0-4.csv'
percentage = 0.9
trials = 200

###

import numpy, heapq, random

def run(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, generations, diff_mate_prob_A, result_file):
    age_gen = numpy.random.default_rng()
    
    population = [(age_gen.normal(mean_ages['a'], age_sds['a']), 'a') for _ in range(start_a_num)]
    population += [(age_gen.normal(mean_ages['b'], age_sds['b']), 'b') for _ in range(start_b_num)]
    heapq.heapify(population)
    
    counts_over_time = []
    curr_counts = {'a': start_a_num, 'b': start_b_num}
    
    writeme = 't,a,b'
    
    for t in range(0, generations, time_step):
        n_mates = 0
        # kill
        while len(population) > 0 and population[0][0] < t:
            (_, kind) = heapq.heappop(population)
            curr_counts[kind] -= 1
            n_mates += 1
        if len(population) <= 1:
            print('oops extinct')
            print('population:', population)
            break
        # mate
        to_add = []
        while len(to_add) < n_mates:
            [(_, kind0), (_, kind1)] = random.sample(population, 2)
            roll = random.random()
            if kind0 == kind1 and roll <= prob_same_mate:
                new = (age_gen.normal(mean_ages[kind0], age_sds[kind0]) + t, kind0)
                to_add.append(new)
                curr_counts[kind0] += 1
            elif kind0 != kind1 and roll <= prob_diff_mate:
                if random.random() <= diff_mate_prob_A:
                    new_kind = 'a'
                else:
                    new_kind = 'b'
                new = (age_gen.normal(mean_ages[new_kind], age_sds[new_kind]) + t, new_kind)
                to_add.append(new)
                curr_counts[new_kind] += 1
        for x in to_add:
            heapq.heappush(population, x)
        
        writeme += f'\n{t},{curr_counts["a"]},{curr_counts["b"]}'
    
    with open(result_file, 'x') as f:
        f.write(writeme)

# for d in [3, 6, 7, 8, 9, 10]:
#     for s in range(d, 11):
#         result_file = f'0-{s}-0-{d}.csv'
#         prob_diff_mate = d / 10
#         prob_same_mate = s / 10
#         run(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_A_num, start_B_num, time_step, generations, diff_mate_prob_A, result_file)

def time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage):
    age_gen = numpy.random.default_rng()
    
    population = [(age_gen.normal(mean_ages['a'], age_sds['a']), 'a') for _ in range(start_a_num)]
    population += [(age_gen.normal(mean_ages['b'], age_sds['b']), 'b') for _ in range(start_b_num)]
    heapq.heapify(population)
    
    counts_over_time = []
    curr_counts = {'a': start_a_num, 'b': start_b_num}
    
    # for t in range(0, generations, time_step):
    t = -1
    while True:
        t += 1
        n_mates = 0
        # kill
        while len(population) > 0 and population[0][0] < t:
            (_, kind) = heapq.heappop(population)
            curr_counts[kind] -= 1
            n_mates += 1
        if len(population) <= 1:
            print('oops extinct')
            print('population:', curr_counts)
            break
        # mate
        to_add = []
        while len(to_add) < n_mates:
            [(_, kind0), (_, kind1)] = random.sample(population, 2)
            roll = random.random()
            if kind0 == kind1 and roll <= prob_same_mate:
                new = (age_gen.normal(mean_ages[kind0], age_sds[kind0]) + t, kind0)
                to_add.append(new)
                curr_counts[kind0] += 1
            elif kind0 != kind1 and roll <= prob_diff_mate:
                if random.random() <= diff_mate_prob_A:
                    new_kind = 'a'
                else:
                    new_kind = 'b'
                new = (age_gen.normal(mean_ages[new_kind], age_sds[new_kind]) + t, new_kind)
                to_add.append(new)
                curr_counts[new_kind] += 1
        for x in to_add:
            heapq.heappush(population, x)
        # stop
        a_percent = curr_counts['a'] / (start_a_num + start_b_num)
        if a_percent >= percentage or a_percent < (1 - percentage):
            assert(a_percent < (1 - percentage))
            # print('reached percentage!')
            # print('population:', curr_counts)
            return t
        
def avg_time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage, trials):
    total = 0
    for _ in range(trials):
        total += time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage)
    return total / trials

f = open('mcdata200.csv', 'w')
for d in range(3, 11):
    for s in range(3, 11):
        if s < d:
            t = 0
        else:
            prob_diff_mate = d / 10
            prob_same_mate = s / 10
            t = avg_time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage, trials)
        writed = f'0.{d}'
        writes = f'0.{s}'
        if d == 10:
            writed = '1.0'
        if s == 10:
            writes = '1.0'
        f.write(f'\n{t}')
        print(f'\n{writed},{writes},{t}')

f.close()


# print(avg_time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage, trials))

    







