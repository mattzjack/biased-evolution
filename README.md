# Biased Evolution

class project for CMU 02-331 Modeling Evolution (2020 spring), Dr. Oana Carja

## overview

2 genotypes A and B, with mating bias. A likes mating with A; B likes mating with B.

2 models:
- infinite population: deterministic
- finite population: with probabilities

each round:
1. 

