# parametres

mean_ages = Dict( 'a'=> 10, 'b'=> 20 )
age_sds = Dict( 'a'=> 3, 'b'=> 3 )
prob_same_mate = 0.4
prob_diff_mate = 0.4
start_a_num = 100
start_b_num = 100
time_step = 1
generations = 100
diff_mate_prob_A = 0.5
result_file = "./1-0-0-4.csv"
percentage = 0.9
trials = 200

###


using Distributions

function time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, 
                              start_b_num, time_step, diff_mate_prob_A, percentage)
    aN = Normal(mean_ages['a'], age_sds['b'])
    bN = Normal(mean_ages['a'], age_sds['b'])
    
    population = [(rand(aN), 'a') for _ in 1:start_a_num]
    append!(population, [(rand(bN), 'b') for _ in 1:start_b_num])
    sort!(population)
    curr_counts = Dict( 'a'=> start_a_num, 'b'=> start_b_num )
    
    t = -1
    while true
        t += 1
        n_mates = 0
        # kill
        while length(population) > 0 && population[1][1] < t
            (_, kind) = popfirst!(population)
            curr_counts[kind] -= 1
            n_mates += 1
        end
        if length(population) <= 1
            println("oops extinct")
            println("population:", curr_counts)
            break
        end
        # mate
        to_add = Tuple{Float64,Char}[]
        while length(to_add) < n_mates
            aa, bb = rand(population, 2)
            kind0 = aa[2]
            kind1 = bb[2]
            roll = rand()
            if kind0 == kind1 && roll <= prob_same_mate
                new = (rand(Normal(mean_ages[kind0], age_sds[kind0])) + t, kind0)
                push!(to_add, new)
                curr_counts[kind0] += 1
            elseif kind0 != kind1 && roll <= prob_diff_mate
                if rand() <= diff_mate_prob_A
                    new_kind = 'a'
                else
                    new_kind = 'b'
                end
                new = (rand(Normal(mean_ages[new_kind], age_sds[new_kind])) + t, new_kind)
                push!(to_add, new)
                curr_counts[new_kind] += 1
            end
        end
        append!(population, to_add)
        sort!(population)
        # stop
        a_percent = curr_counts['a'] / (start_a_num + start_b_num)
        if a_percent >= percentage || a_percent < (1 - percentage)
            @assert a_percent < (1 - percentage)
            # print('reached percentage!')
            # print('population:', curr_counts)
            return t
        end
    end
end
        
function avg_time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, 
                             start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage, trials)
    total = 0
    for _ in 0:trials
        total += time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage)
    end
    return total / trials
end

function final()
    open("myass.txt", "w") do io
        for d in 3:10
            for s in 3:10
                if s < d
                    t = 0
                else
                    prob_diff_mate = d / 10
                    prob_same_mate = s / 10
                    t = avg_time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage, trials)
                end
                writed = "0.$d"
                writes = "0.$s"
                if d == 10
                    writed = "1.0"
                end
                if s == 10
                    writes = "1.0"
                end
                write(io, "\n$t")
                println("\n$writed,$writes,$t")
            end
        end
    end
end

#= f.close() =#


#= # print(avg_time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, start_b_num, time_step, diff_mate_prob_A, percentage, trials)) =#

    
#= time_till_percentage(mean_ages, age_sds, prob_same_mate, prob_diff_mate, start_a_num, =# 
                              #= start_b_num, time_step, diff_mate_prob_A, percentage) =#





